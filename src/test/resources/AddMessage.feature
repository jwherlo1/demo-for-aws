Feature: a message can be added

  Scenario: adding a message adds to persistence store

    When the user adds the message "test message 1"

    Then the message "test message 1" is persisted