package com.example.demo.cucumber;

import com.example.demo.Message;
import com.example.demo.MessageService;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class MessageStepDefs {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private MessageService messageService;

    @When("the user adds the message {string}")
    public void theUserAddsTheMessage(String message) {
        ResponseEntity<String> response = testRestTemplate.postForEntity("/message/add", message, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Then("the message {string} is persisted")
    public void theMessageIsPersisted(String message) {
        messageService.getMessages().contains(message);
    }

    @Then("the message {string} can be listed")
    public void theMessageCanBeListed(String message) {
        ResponseEntity<Message[]> response = testRestTemplate.getForEntity("/message/list", Message[].class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();

        List<String> messages = Stream.of(response.getBody())
                .map(Message::getMessage)
                .collect(Collectors.toList());

        assertThat(messages).contains(message);
    }
}
