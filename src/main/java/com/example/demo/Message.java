package com.example.demo;

import java.time.LocalDateTime;

public class Message {

    private String message;

    private LocalDateTime created;

    public Message(String message, LocalDateTime created) {
        this.message = message;
        this.created = created;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", created=" + created +
                '}';
    }
}
