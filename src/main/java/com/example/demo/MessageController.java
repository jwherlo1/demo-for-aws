package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/message/add")
    public void addMessage(@RequestBody String message) {
        messageService.addMessage(message);
    }

    @GetMapping("/message/list")
    public List<Message> listMessages() {
        return messageService.getMessages();
    }
}
