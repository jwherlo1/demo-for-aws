package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity(name = "message")
public class MessageEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String text;

    private LocalDateTime created;

    public MessageEntity() {
    }

    public MessageEntity(String message) {
        text = message;
        created = LocalDateTime.now();
    }

    public MessageEntity(String text, LocalDateTime created) {
        this.text = text;
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    @Override
    public String toString() {
        return "MessageEntity{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", created=" + created +
                '}';
    }
}
