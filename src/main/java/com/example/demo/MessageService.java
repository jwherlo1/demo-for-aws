package com.example.demo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Service
public class MessageService {

    private MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public void addMessage(String message) {
        messageRepository.save(new MessageEntity(message));
    }

    public List<Message> getMessages() {
        List<Message> messages = new ArrayList<>();
        messageRepository.findAll().forEach(entityToMessage(messages));
        return messages;
    }

    private Consumer<MessageEntity> entityToMessage(List<Message> messages) {
        return message -> messages.add(new Message(message.getText(), message.getCreated()));
    }
}
